# Crawler Dockerfile

FROM python:3.6.0-alpine

COPY . /app
RUN pip install -r /app/requirements.txt

WORKDIR /app

ENV MONGO_DATABASE_HOST 127.0.0.1
ENV MONGO_DATABASE_PORT 27017
ENV RMQ_HOST 127.0.0.1
ENV RMQ_QUEUE "crawler"
ENV RMQ_USERNAME "guest"
ENV RMQ_PASSWORD "guest"
ENV CHECK_INTERVAL 5
ENV EXCLUDE_URLS "" 
ENV DEFAULT_URL ""

CMD ["sh", "-c", "python -u crawler/crawler.py ${DEFAULT_URL}"]
